# CI / CD

GitLab Runner erabiliko dugu. Besteak beste, lanak docker edukiontzitan exekutatzeko aukera ematen digu.


## basic

``` yaml
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/hexo
image: node:9-alpine

pages:
  script:
  - yarn global add vuepress
  - vuepress build -d public
  artifacts:
    paths:
    - public
```


## full throttle

``` yaml
stages:
  - build:app
  - build:image
  - deploy

build:
  image: node:9-alpine
  stage: build:app
  script:
  - yarn global add vuepress
  - vuepress build -d public
  artifacts:
    paths:
    - public

build:pages:
  image: node:9-alpine
  stage: build:app
  script:
  - yarn global add vuepress
  - DEPLOY_ENV=gitlab-pages vuepress build -d public
  artifacts:
    paths:
    - public

build-image:master:
  image: docker
  stage: build:image
  dependencies:
    - build
  services:
    - docker:dind
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - master

build-image:
  image: docker
  stage: build:image
  dependencies:
    - build
  services:
    - docker:dind
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG"
  except:
    - master

pages:
  stage: deploy
  image: busybox
  script:
    - echo WADDUP!
  dependencies:
    - build:pages
  artifacts:
    paths:
    - public

production:
  stage: deploy
  image: registry.gitlab.com/jimakker/alpine-kubectl:latest
  only:
    - tags
    - /^v.*$/
  when: manual
  environment:
    name: production
    url: http://test.k8s.dev.talaios.net
  script:
    - mkdir ~/.kube
    - echo "$KUBE_CONFIG" > ~/.kube/config
    - kubectl --kubeconfig='/root/.kube/config' set image deployment/docs docs=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

```
