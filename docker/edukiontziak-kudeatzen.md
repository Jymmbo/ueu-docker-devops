# Edukiontziak kudeatzen

## docker ps

Edukiontziak zerrendatzen ditu.

* `-a`: (`--all`) Geldirik daudenak ere bai
* `-q`: (`--quiet`) ID-ak bakarrik itzuli

::: tip ARIKETA
Aukera ezberdinak erabilita zerrendatu orain arte sortutako bi edukiontziak.
:::

## docker inspect

Edukiontziaren informazioa ematen du.

::: tip ARIKETA
Martxan dagoen `nginx` edukiontziaren IP-a topatu eta nabigatzailean ireki.
:::

## docker attach

Atzekaldean exekutatzen ari den edukiontzi bati atxikitu.

::: tip ARIKETA
Martxan dagoen `nginx` edukiontziari atxikitu.
:::

::: warning ADI
`CTRL-c`-k edukiontzia geldituko du. Erabili `CTRL-p CTRL-q` edukiontziki destxikitzeko.
:::

## docker start

Edukiontzia(k) abiarazten d(it)u.

::: tip ARIKETA
Hasieran sortu dugun `nginx` edukiontzia martxan jarri.
:::

## docker stop

Edukiontzia(k) gelditzen d(it)u.

::: tip ARIKETA
Martxan dagoen `nginx` edukiontzia gelditu.
:::

## docker exec

Komando bat exekutatu edukiontzian.

::: tip ARIKETA
Martxan dagoen `nginx` edukiontzian `top` exekutatu.
:::

## docker rm

Edukiontziak ezabatzen ditu.

::: tip ARIKETA
Edukiontzi guztiak ezabatu.
:::

::: tip TIP
`docker help` erabili komando eta aukera guztiak ikusteko. Ikusi ditugun komado hauek `docker container`-ekoen *ezizenak* dira `docker help container` egin dezakezu komando eta aukera guztiak ikusteko.
:::

## docker container prune

[https://docs.docker.com/config/pruning/#prune-containers](https://docs.docker.com/config/pruning/#prune-containers)

Geldirik dauden edukiontziak ezabatu.
